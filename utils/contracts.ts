// FETCH nfts url while loading
import { ethers } from 'ethers'
import { avax, sepolia } from "../constants/contracts"
import contractJson from '~/contracts/GRIT404.json'

const provider = new ethers.providers.JsonRpcProvider(sepolia.rpcProvider)

// load deployed contracts from local storage
let deployedContracts = JSON.parse(localStorage.getItem("deployedContracts") || "[]")
if (deployedContracts.length == 0) {
    deployedContracts = sepolia.contractAddresses
    localStorage.setItem("deployedContracts", JSON.stringify(deployedContracts));
}

const contracts = deployedContracts.map((contractAddress: string) => {
    return {
        contract: new ethers.Contract(contractAddress, sepolia.erc404ABI, provider),
        contractAddress: contractAddress // TODO: get contract addresses from local storage
    }
})
// console.debug(contracts.length)

export const getNFTsOfOwner = async (ownerAddress: any) => {
    console.debug("getNFTsOfOwner:", ownerAddress)

    // get all ownded tokens id
    const owned = (await Promise.all(contracts.map(async (contract: any, idx: number) => {
        const ownedNFTs = await contract.contract.owned(ownerAddress)
        return ownedNFTs.map((nftId: any) => {
            return { nftId: nftId, contractIdx: idx }
        })
    }))).flat()

    // get tokens uri
    const prefix = "data:application/json;utf8"
    const nftsData = await Promise.all(owned.map(async (element: any, idx: number) => {
        try {
            if (idx == 0) {
                console.debug("element: ", element)
            }

            const tokenURI = await contracts[element.contractIdx].contract.tokenURI(element.nftId)

            return { ...JSON.parse(String(tokenURI).substring(prefix.length + 1)), id: Number(element.nftId), contractIdx: element.contractIdx } // TODO: safe convert bigint to number
        } catch (error) {
            console.debug("get tokens uri errors:", error)
            return undefined
        }

    }))
    console.log(`nftsData: length=${nftsData.length}, first=${JSON.stringify(nftsData[0])}`);

    return nftsData
}

// 0x0cBD0750C9Ab0324048F7354cf4653212C23B507
export const sendNFT = async (from: string, to: string, tokenID: number, contractIdx: number) => {
    // TODO: get provider and signer from metamask sdk
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = await provider.getSigner(from)

    const contract = new ethers.Contract(sepolia.contractAddresses[contractIdx], sepolia.erc404ABI, signer)

    const result = await contract["safeTransferFrom(address,address,uint256)"](from, to, tokenID)
    return result
}
export const deployContract = async (formData: any, deployerAddres: string) => {

    console.log('Start deploying')
    try {

        const provider = new ethers.providers.Web3Provider(window.ethereum)
        const signer = await provider.getSigner(deployerAddres)


        const ERC721Contract = ethers.ContractFactory.fromSolidity(contractJson, signer);
        const erc721Contract = await ERC721Contract.deploy(formData.dataUri, formData.name, formData.symbol, formData.decimals, formData.totalSupply, deployerAddres);

        const contractDeployed = await erc721Contract.deployed();
        console.log(contractDeployed);
        console.log('Contract deployed to address:', erc721Contract.address);

        deployedContracts = [...deployedContracts, erc721Contract.address]
        localStorage.setItem("deployedContracts", JSON.stringify(deployedContracts));

        return { isSuccess: true, deployedAddress: erc721Contract.address }
    } catch (error) {
        console.error(error)
        return { isSuccess: false }
    }
}