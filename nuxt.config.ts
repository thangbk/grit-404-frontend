// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr      : false,
    devtools : {enabled: false},
    modules  : [
        '@pinia/nuxt',
        '@nuxt/ui'
    ],
    imports  : {
        dirs: ['stores']
    },
    plugins  : [
        '~/plugins/wallet-connect'
    ],
    devServer: {
        port: 3000, // default: 3000
        host: '0.0.0.0' // default: localhost
    },
    colorMode: {
        preference: 'light'
    }
})
